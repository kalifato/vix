ARG BASE_IMAGE="debian:bullseye-slim"
FROM ${BASE_IMAGE} as builder

RUN set -ex; \
    export DEBIAN_FRONTEND="noninteractive"; \
    apt-get -yq update; \
    apt-get -yq full-upgrade; \
    # Install dependencies
    apt-get install -yqq build-essential autotools-dev autoconf automake1.11 libsdl-image1.2-dev libtool git; \
    cd /tmp; \
    git clone https://github.com/BatchDrake/vix; \
    cd vix; \
    autoreconf -vfi; ./configure --prefix /opt/vix;  make;  make install; \
    rm -rf /tmp/vix-src

FROM ${BASE_IMAGE} as runtime

# Default user and group identifiers
ARG USER_UID="1000"
ARG USER_GID="1000"

WORKDIR /opt/vix

COPY --from=builder /opt/vix .

RUN set -ex; \
    export DEBIAN_FRONTEND="noninteractive"; \
    apt-get -yq update; \
    apt-get -yq full-upgrade; \
    # Install dependencies
    apt-get install -yqq libsdl-image1.2; \
        apt-get -yq clean; \
    apt-get -yq autoclean; \
    apt-get -yq autoremove; \
    rm -rf /var/lib/apt/lists/*;

# Create group and user
RUN groupadd --gid "${USER_GID}" "group";
RUN useradd -m user --uid "${USER_UID}" --gid "${USER_GID}"

WORKDIR /home/vix

ENTRYPOINT [ "/opt/vix/bin/vix" ]
