#!/bin/sh

docker run -it --rm \
    -e SDL_VIDEODRIVER=x11 \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY=$DISPLAY \
    -v $(pwd):/home/vix \
    --name vix \
    registry.gitlab.com/kalifato/vix:latest \
    $@

