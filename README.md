# Vix docker

This repository contains tools to create a vix docker image.

Vix is a tool created by BatchDrake original repository is 
located at [github](https://github.com/BatchDrake/vix).

## Docker image manual creation
If you want to create your own vix image you must type this in a shell:
```
docker build -t vix .
```

## Usage

To make easier usage, a run script is provided. This script will pull vix from gitlab registry and them run it.

There are several two ways you can run it, with or without params.

1. __Run with default values__
```
#    ./run.sh
```
or
```
#    ./run.sh vix
```

2. __Run with custom params or filename__

```
#    ./run.sh vix param1 param2 ....
```
